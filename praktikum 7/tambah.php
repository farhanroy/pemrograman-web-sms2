<!DOCTYPE html>
<html>

<head>
	<title>Tambah Mahasiswa</title>
</head>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

<body>
	<div class="container col-md-6 mt-4">
		<h1>Mahasiswa PENS</h1>
		<div class="card">
			<div class="card-header bg-dark text-white">
				Tambah Mahasiswa
			</div>
			<div class="card-body">
				<form action="" method="post" role="form">
                    <div class="form-group">
						<label>NRP</label>
						<input type="text" name="nrp" required="" class="form-control">
					</div>
					<div class="form-group mt-3">
						<label>Nama</label>
						<input type="text" name="nama" required="" class="form-control">
					</div>
                    <div class="form-group mt-3">
						<label>Jenis Kelamin</label>
						<select class="form-control" name="jenis_kelamin">
                            <option value="Laki - Laki">Laki - Laki</option>
                            <option value="Perempuan">Perempuan</option>
                        </select>
					</div>
					<div class="form-group mt-3">
					<?php
						include('koneksi.php');
                          $sql="select * from jurusan";

                          $hasil=mysqli_query($koneksi,$sql);
                          $no=0;
                          while ($data = mysqli_fetch_array($hasil)) {
                            $no++;
                    ?>
					<label>Jurusan</label>
						<select class="form-control" name="jurusan">
                            <option value="<?php echo $data['jurusan'];?>"><?php echo $data['nama'];?></option>
                        </select>
                        <?php } ?>
				
					</div>
					<div class="form-group mt-3">
						<label>Email</label>
						<input class="form-control" name="email_student">
					</div>
                    <div class="form-group mt-3">
						<label>Alamat</label>
						<textarea type="text" name="alamat" class="form-control"></textarea>
					</div>
                    <div class="form-group mt-3">
						<label>No. HP</label>
						<input type="text" name="no_hp" class="form-control">
					</div>
                    <div class="form-group mt-3">
						<label>Asal Sekolah</label>
						<input type="text" name="asal_sma" class="form-control">
					</div>
                    <div class="form-group mt-3">
						<label>Matkul Favorit</label>
						<input type="text" name="matkul_favorit" class="form-control">
					</div>
                    <div class="form-group mt-3">
						<label>Wadah Pengembangan</label>
						<input type="text" name="wadah_pengembangan" class="form-control">
					</div>
					<button type="submit" class="btn btn-success mt-4" name="submit" value="simpan">Simpan data</button>
				</form>

				<?php
				
				if (isset($_POST['submit'])) {
                    $nrp = $_POST['nrp'];
                    $nama = $_POST['nama'];
                    $jenis_kelamin = $_POST['jenis_kelamin'];
                    $jurusan = $_POST['jurusan'];
                    $email_student = $_POST['email_student'];
                    $alamat = $_POST['alamat'];
                    $no_hp = $_POST['no_hp'];
                    $asal_sma = $_POST['asal_sma'];
                    $matkul_favorit = $_POST['matkul_favorit'];
                    $wadah_pengembangan = $_POST['wadah_pengembangan'];

					$datas = mysqli_query($koneksi, "insert into mahasiswa (nrp,nama,jenis_kelamin,jurusan,email_student, alamat, no_hp, asal_sma, matkul_favorit, wadah_pengembangan)values('$nrp', '$nama', '$jenis_kelamin', '$jurusan', '$email_student', '$alamat', '$no_hp', '$asal_sma', '$matkul_favorit', '$wadah_pengembangan')") or die(mysqli_error($koneksi));
                    
                    // Membuat alert ketika data sukses diinputkan
					echo "<script>alert('data berhasil disimpan.');window.location='index.php';</script>";
				}
				?>
			</div>
		</div>
	</div>

	<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
</body>

</html>