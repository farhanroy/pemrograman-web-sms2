<!DOCTYPE html>
<html>

<head>
	<title>Update Mahasiswa</title>
</head>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

<body>
	<div class="container col-md-6 mt-4">
		<h1>Mahasiswa PENS</h1>
		<div class="card">
			<div class="card-header bg-dark text-white">
				Update Mahasiswa
			</div>
			<div class="card-body">
                <?php 
                    include('koneksi.php');

                    $no = $_GET['no'];

                    $data = mysqli_query($koneksi, "select * from mahasiswa where no = '$no'");
                    $row = mysqli_fetch_assoc($data);
                ?>
				<form action="" method="post" role="form">
                    <div class="form-group">
						<label>NRP</label>
						<input type="text" name="nrp" required="" class="form-control" value="<?= $row['nrp']; ?>">
					</div>
					<div class="form-group mt-3">
						<label>Nama</label>
						<input type="text" name="nama" required="" class="form-control" value="<?= $row['nama']; ?>">
					</div>
                    <div class="form-group mt-3">
						<label>Jenis Kelamin</label>
						<select class="form-control" name="jenis_kelamin">
                            <option value="Laki - Laki">Laki - Laki</option>
                            <option value="Perempuan">Perempuan</option>
                        </select>
					</div>
					<div class="form-group mt-3">
                    <?php
                          $sql="select * from jurusan";

                          $hasil=mysqli_query($koneksi,$sql);
                          $no=0;
                          while ($data = mysqli_fetch_array($hasil)) {
                            $no++;
                    ?>
                    <label>Jurusan</label>
						<select class="form-control" name="jurusan">
                            <option value="<?php echo $data['jurusan'];?>"><?php echo $data['nama'];?></option>
                        </select>
                          <?php } ?>
					</div>
					<div class="form-group mt-3">
						<label>Email</label>
						<input class="form-control" name="email_student" value="<?= $row['email_student']; ?>">
					</div>
                    <div class="form-group mt-3">
						<label>Alamat</label>
						<textarea type="text" name="alamat" class="form-control"><?= $row['alamat']; ?></textarea>
					</div>
                    <div class="form-group mt-3">
						<label>No. HP</label>
						<input type="text" name="no_hp" class="form-control" value="<?= $row['no_hp']; ?>">
					</div>
                    <div class="form-group mt-3">
						<label>Asal Sekolah</label>
						<input type="text" name="asal_sma" class="form-control" value="<?= $row['asal_sma']; ?>">
					</div>
                    <div class="form-group mt-3">
						<label>Matkul Favorit</label>
						<input type="text" name="matkul_favorit" class="form-control" value="<?= $row['matkul_favorit']; ?>">
					</div>
                    <div class="form-group mt-3">
						<label>Wadah Pengembangan</label>
						<input type="text" name="wadah_pengembangan" class="form-control" value="<?= $row['wadah_pengembangan']; ?>">
					</div>
					<button type="submit" class="btn btn-success mt-4" name="submit" value="simpan">Update data</button>
				</form>

				<?php
				
				if (isset($_POST['submit'])) {
                    $nrp = $_POST['nrp'];
                    $nama = $_POST['nama'];
                    $jenis_kelamin = $_POST['jenis_kelamin'];
                    $jurusan = $_POST['jurusan'];
                    $email_student = $_POST['email_student'];
                    $alamat = $_POST['alamat'];
                    $no_hp = $_POST['no_hp'];
                    $asal_sma = $_POST['asal_sma'];
                    $matkul_favorit = $_POST['matkul_favorit'];
                    $wadah_pengembangan = $_POST['wadah_pengembangan'];

					$datas = mysqli_query($koneksi, "update mahasiswa set nrp='$nrp',nama='$nama',jenis_kelamin='$jenis_kelamin',jurusan='$jurusan',email_student='$email_student', alamat='$alamat', no_hp='$no_hp', asal_sma='$asal_sma', matkul_favorit='$matkul_favorit', wadah_pengembangan='$wadah_pengembangan' where no='$no' ") or die(mysqli_error($koneksi));
                    
                    // Membuat alert ketika data sukses diinputkan
					echo "<script>alert('data berhasil diupdate.');window.location='index.php';</script>";
				}
				?>
			</div>
		</div>
	</div>

	<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
</body>

</html>