<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <title>Mahasiswa</title>
  </head>
  <body>

  	<div class="container col-xl-12 mt-4">
		<h1>MAHASISWA PENS</h1>
		<div class="card mt-4">
			<div class="card-header bg-dark text-white ">
				<a href="tambah.php" class="btn btn-sm btn-success float-right">Tambah</a>
			</div>
			<div class="card-body">
				<table class="table table-bordered">
					<thead>
						<tr>
							<th>No</th>
							<th>NRP</th>
							<th>Nama</th>
							<th>Jenis Kelamin</th>
							<th>Kode Jurusan</th>
                            <th>Email</th>
                            <th>Alamat</th>
                            <th>No. Handphone</th>
                            <th>Asal SMA/SMK</th>
                            <th>Matkul Favorit</th>
                            <th>Wadah Pengembangan</th>
                            <th>Aksi</th>
						</tr>
					</thead>

					<tbody>
                        <?php
                            include('koneksi.php');
                            $datas = mysqli_query($koneksi, "select * from mahasiswa") or die(mysqli_error($koneksi));
                            
                            $nomor = 1;
                            while($row = mysqli_fetch_assoc($datas)) {
                        ?>
                        <tr>
                            <td><?=$row['no'];?></td>
                            <td><?= $row['nrp'];?></td>
                            <td><?= $row['nama'];?></td>
                            <td><?= $row['jenis_kelamin']; ?></td>
                            <td><?= $row['jurusan']; ?></td>
                            <td><?= $row['email_student']; ?></td>
                            <td><?= $row['alamat']; ?></td>
                            <td><?= $row['no_hp']; ?></td>
                            <td><?= $row['asal_sma']; ?></td>
                            <td><?= $row['matkul_favorit']; ?></td>
                            <td><?= $row['wadah_pengembangan']; ?></td>
                            <td>
                                    <a href="edit.php?no=<?= $row['no']; ?>" class="btn btn-sm btn-warning">Edit</a>
                                    <a href="hapus.php?no=<?= $row['no']; ?>" class="btn btn-sm btn-danger" onclick="return confirm('anda yakin ingin hapus?');">Hapus</a>
                            </td>
					    </tr>

						<?php $no++; } ?>
					</tbody>

				</table>
			</div>
		</div>
	</div>
    
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>

  </body>
</html>
