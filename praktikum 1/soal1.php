<?php

$n = 10;
$f1 = 1; 
$f2 = 1;
$next = 0;

for ($i = 1; $i <= 10; $i++) {
    if ($i == 1) {
        echo "$f1 ";
        continue;
    } 

    if ($i == 2) {
        echo "$f2 ";
        continue;
    }

    $next = $f1 + $f2;
    $f1 = $f2;
    $f2 = $next;
    echo "$next ";
}

?>