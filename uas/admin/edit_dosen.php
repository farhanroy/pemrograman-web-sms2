<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Admin | E-Learning</title>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
</head>
<body>
    <nav class="navbar navbar-expand-lg bg-light">
        <div class="container-fluid">
            <a class="navbar-brand" href="#">E-Learning</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavDropdown">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="nav-link" aria-current="page" href="#">Dashboard</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" href="#">Dosen</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Mahasiswa</a>
                    </li>
                    
                </ul>
            </div>
        </div>
    </nav>

    <div class="container mt-5">
        <div class="row">
            <div class="col">
                <h2>Update Dosen</h2>
            </div>
        </div>

        <?php
        include '../config/database.php';
        
        $nip = $_GET['nip'];

        $result1 = mysqli_query($koneksi, "select * from users where nomor_induk = '$nip'");
        $result2 = mysqli_query($koneksi, "select * from dosen where nip = '$nip'");
        $datas1 = mysqli_fetch_assoc($result1);
        $datas2 = mysqli_fetch_assoc($result2);
        ?>

        <div class="row mt-4">
            <div class="col-7">
                <form method="POST" action="./actions/edit_dosen_action.php" enctype="multipart/form-data">
                    <div class="mb-3">
                        <label for="exampleInputEmail1" class="form-label">Email</label>
                        <input name="email" type="email" class="form-control" value="<?= $datas1['email']; ?>">
                    </div>
                    <div class="mb-3">
                        <label for="exampleInputPassword1" class="form-label">Password</label>
                        <input name="password" type="text" class="form-control" value="<?= $datas1['password']; ?>">
                    </div>
                    <div class="mb-3">
                        <label for="exampleInputEmail1" class="form-label">NIP</label>
                        <input name="nip" type="text" class="form-control" value="<?= $datas1['nomor_induk']; ?>">
                    </div>
                    <div class="mb-3">
                        <label for="exampleInputPassword1" class="form-label">Nama</label>
                        <input name="nama" type="text" class="form-control" value="<?= $datas2['nama']; ?>">
                    </div>
                    <div class="mb-3">
                        <label for="exampleInputEmail1" class="form-label">Program Pendidikan</label>
                        <input name="prodi" type="text" class="form-control" value="<?= $datas2['prodi']; ?>">
                    </div>
                    <div class="mb-3">
                        <label for="exampleInputPassword1" class="form-label">Alamat</label>
                        <input name="alamat" type="text" class="form-control" value="<?= $datas2['alamat']; ?>">
                    </div>
                    <div class="mb-3">
                        <label for="exampleInputEmail1" class="form-label">Tanggal Lahir</label>
                        <input name="tanggal" type="date" class="form-control" value="<?= $datas2['tanggal_lahir']; ?>">
                    </div>
                    <div class="mb-3">
                        <label for="exampleInputPassword1" class="form-label">Pas Foto</label>
                        <input type="file" name="foto" class="form-control"  />
                    </div>
                    <button type="submit" class="btn btn-primary mt-3">Kirim</button>
                </form>
            </div>
        </div>
    </div>
    
</body>
</html>