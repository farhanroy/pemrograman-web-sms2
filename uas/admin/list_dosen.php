
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Admin | E-Learning</title>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
</head>
<body>
    <nav class="navbar navbar-expand-lg bg-light">
        <div class="container-fluid">
            <a class="navbar-brand" href="#">E-Learning</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavDropdown">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="#">Dashboard</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Dosen</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Mahasiswa</a>
                    </li>
                    
                </ul>
            </div>
        </div>
    </nav>
    <div class="container mt-5">
        <div class="row">
            <div class="col-6">
                <h2>Daftar Dosen</h2>
            </div>

            <div class="col-6 text-end">
                <a class="btn btn-success" href="./tambah_dosen.php">Tambah</a>
            </div>
        </div>

        <div class="row mt-5">
            <div class="col-12 mx-auto">
                <table class="table">
                    <thead>
                        <tr>
                        <th scope="col">NIP</th>
                        <th scope="col">Nama</th>
                        <th scope="col">Prodi</th>
                        <th scope="col">Tanggal Lahir</th>
                        <th scope="col">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        include '../config/database.php';
                        $result = mysqli_query($koneksi, 'select * from dosen');
                        while($row = mysqli_fetch_assoc($result)) {
                        ?>
                        <tr>
                            
                            <td><?= $row['nip']; ?></td>
                            <td><?= $row['nama']; ?></td>
                            <td><?= $row['prodi']; ?></td>
                            <td><?= $row['tanggal_lahir']; ?></td>
                            <td>
                                <div class="row justify-content-start">
                                    <div class="col">
                                    <a class="btn btn-primary" href="./tambah_dosen.php">Lihat</a>
                                    <a class="btn btn-warning" href="./edit_dosen.php?nip=<?= $row['nip']; ?>">Edit</a>
                                    <a class="btn btn-danger" href="./actions/hapus_dosen_action.php?nip=<?= $row['nip']; ?>">Hapus</a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <?php 
                        } 
                        mysqli_close();
                        ?> 
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</body>
</html>