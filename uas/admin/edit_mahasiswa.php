<?php include('./components/header.php'); ?>
<body>
    <?php include('./components/navbar.php'); ?>

    <div class="container mt-5">
        <div class="row">
            <div class="col">
                <h2>Update Mahasiswa</h2>
            </div>
        </div>

        <?php
        include '../config/database.php';
        
        $nrp = $_GET['nrp'];

        $result1 = mysqli_query($koneksi, "select * from users where nomor_induk = '$nrp'");
        $result2 = mysqli_query($koneksi, "select * from mahasiswa where nrp = '$nrp'");
        $datas1 = mysqli_fetch_assoc($result1);
        $datas2 = mysqli_fetch_assoc($result2);

        ?>

        <div class="row mt-4">
            <div class="col-7">
                <form method="POST" action="./actions/edit_mahasiswa_action.php" enctype="multipart/form-data">
                    <div class="mb-3">
                        <label for="exampleInputEmail1" class="form-label">Email</label>
                        <input name="email" type="email" class="form-control" value="<?= $datas1['email']; ?>">
                    </div>
                    <div class="mb-3">
                        <label for="exampleInputPassword1" class="form-label">Password</label>
                        <input name="password" type="text" class="form-control" value="<?= $datas1['password']; ?>">
                    </div>
                    <div class="mb-3">
                        <label for="exampleInputEmail1" class="form-label">NIP</label>
                        <input name="nrp" type="text" class="form-control" value="<?= $datas1['nomor_induk']; ?>">
                    </div>
                    <div class="mb-3">
                        <label for="exampleInputPassword1" class="form-label">Nama</label>
                        <input name="nama" type="text" class="form-control" value="<?= $datas2['nama']; ?>">
                    </div>
                    <div class="mb-3">
                        <label for="exampleInputEmail1" class="form-label">Kelas</label>
                        <input name="kelas" type="text" class="form-control" value="<?= $datas2['kelas']; ?>">
                    </div>
                    <div class="mb-3">
                        <label for="exampleInputPassword1" class="form-label">Alamat</label>
                        <input name="alamat" type="text" class="form-control" value="<?= $datas2['alamat']; ?>">
                    </div>
                    <div class="mb-3">
                        <label for="exampleInputEmail1" class="form-label">Tanggal Lahir</label>
                        <input name="tanggal" type="date" class="form-control" value="<?= $datas2['tanggal_lahir']; ?>">
                    </div>
                    <div class="mb-3">
                        <label for="exampleInputPassword1" class="form-label">Pas Foto</label>
                        <input type="file" name="foto" class="form-control"  />
                    </div>
                    <button type="submit" class="btn btn-primary mt-3">Kirim</button>
                </form>
            </div>
        </div>
    </div>
    
</body>
<?php include('./components/footer.php'); ?>