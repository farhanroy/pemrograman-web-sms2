<?php include('./components/header.php') ?>
<body>
    <?php include('./components/navbar.php') ?>

    <div class="container mt-5">
        <div class="row">
            <div class="col">
                <h2>Tambah Mahasiswa</h2>
            </div>
        </div>

        <div class="row mt-4">
            <div class="col-7">
                <form method="POST" action="./actions/tambah_mahasiswa_action.php" enctype="multipart/form-data">
                    <div class="mb-3">
                        <label for="exampleInputEmail1" class="form-label">Email</label>
                        <input name="email" type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                    </div>
                    <div class="mb-3">
                        <label for="exampleInputPassword1" class="form-label">Password</label>
                        <input name="password" type="text" class="form-control" id="exampleInputPassword1">
                    </div>
                    <div class="mb-3">
                        <label for="exampleInputEmail1" class="form-label">NRP</label>
                        <input name="nrp" type="text" class="form-control" >
                    </div>
                    <div class="mb-3">
                        <label for="exampleInputPassword1" class="form-label">Nama</label>
                        <input name="nama" type="text" class="form-control">
                    </div>
                    <div class="mb-3">
                        <label for="exampleInputEmail1" class="form-label">Kelas</label>
                        <input name="kelas" type="text" class="form-control">
                    </div>
                    <div class="mb-3">
                        <label for="exampleInputPassword1" class="form-label">Alamat</label>
                        <input name="alamat" type="text" class="form-control">
                    </div>
                    <div class="mb-3">
                        <label for="exampleInputEmail1" class="form-label">Tanggal Lahir</label>
                        <input name="tanggal" type="date" class="form-control">
                    </div>
                    <div class="mb-3">
                        <label for="exampleInputPassword1" class="form-label">Pas Foto</label>
                        <input type="file" name="foto" class="form-control" />
                    </div>
                    <button type="submit" class="btn btn-primary mt-3">Kirim</button>
                </form>
            </div>
        </div>
    </div>
    
</body>

<?php include('./components/footer.php') ?>