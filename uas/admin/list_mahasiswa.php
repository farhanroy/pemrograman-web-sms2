<?php include('./components/header.php') ?>
<body>
    <?php include('./components/navbar.php') ?>
    <div class="container mt-5">
        <div class="row">
            <div class="col-6">
                <h2>Daftar Mahasiswa</h2>
            </div>

            <div class="col-6 text-end">
                <a class="btn btn-success" href="./tambah_mahasiswa.php">Tambah</a>
            </div>
        </div>

        <div class="row mt-5">
            <div class="col-12 mx-auto">
                <table class="table">
                    <thead>
                        <tr>
                        <th scope="col">NRP</th>
                        <th scope="col">Nama</th>
                        <th scope="col">Kelas</th>
                        <th scope="col">Tanggal Lahir</th>
                        <th scope="col">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        include '../config/database.php';
                        $result = mysqli_query($koneksi, 'select * from mahasiswa');
                        while($row = mysqli_fetch_assoc($result)) {
                        ?>
                        <tr>
                            
                            <td><?= $row['nrp']; ?></td>
                            <td><?= $row['nama']; ?></td>
                            <td><?= $row['kelas']; ?></td>
                            <td><?= $row['tanggal_lahir']; ?></td>
                            <td>
                                <div class="row justify-content-start">
                                    <div class="col">
                                    <a class="btn btn-primary" href="./tambah_mahasiswa.php">Lihat</a>
                                    <a class="btn btn-warning" href="./edit_mahasiswa.php?nrp=<?= $row['nrp']; ?>">Edit</a>
                                    <a class="btn btn-danger" href="./actions/hapus_mahasiswa_action.php?nrp=<?= $row['nrp']; ?>">Hapus</a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <?php 
                        } 
                        mysqli_close();
                        ?> 
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</body>

<?php include('./components/footer.php') ?>