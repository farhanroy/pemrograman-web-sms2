<?php
session_start();

include '../config/database.php';

$email = $_POST['email'];
$password = $_POST['password'];

$result = mysqli_query($koneksi, "select * from users ");

if (mysqli_num_rows($result) > 0) {
  $data = mysqli_fetch_assoc($result);

  if ($data['role'] == 'admin') {
    $_SESSION['email'] = $email;
	$_SESSION['role'] = "admin";
    header("location:../dashboard_admin.php"); 
  } else if ($data['role'] == 'lecturer') {
    $_SESSION['email'] = $email;
	$_SESSION['role'] = "lecturer";
    
  } else if ($data['role'] == 'student') {
    $_SESSION['email'] = $email;
	$_SESSION['role'] = "student";
    
  } else {
    header("location:../login.php?pesan=gagal"); 
  }
} else {
  header("location:../login.php?pesan=gagal"); 
}
?>