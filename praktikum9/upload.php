<?php
include '../library/config.ini';
include '../library/opendb.ini';

error_reporting(0);

$server 	= "localhost";
$username	= "root";
$password	= "";
$db 		= "test";
$koneksi = mysqli_connect($server, $username, $password, $db);

if(mysqli_connect_errno()) {
	echo "Koneksi gagal : ".mysqli_connect_error();
}

$uploadDir = "./image/";

if (isset($_POST['upload'])) {
    $fileName = $_FILES['userfile']['name'];
    $tmpName = $_FILES['userfile']['tmp_name'];
    $fileSize = $_FILES['userfile']['size'];
    $fileType = $_FILES['userfile']['type'];
    $filePath = $uploadDir . $fileName;
    $result = move_uploaded_file($tmpName, $filePath);

    if (!$result) {
        echo "<br> Error uploaded </br>";
        exit;
    }

    $query = "INSERT INTO upload (name, size, type, content, path) VALUES ('$fileName', '$fileSize', '$fileType', '$filePath', '$filePath')";
    mysqli_query($koneksi, $query) or die('Error, gagal input: ' . mysqli_error());

    include '../library/closedb.ini';
    echo "<br> File uploaded <br>";

}
?>
<!DOCTYPE html>
<html>
<head>
<title>Image Upload</title>
<div id="content">
  
    <form action="" method="post" enctype="multipart/form-data" name="uploadform">
        <table width="350" border="0" cellpadding="1" cellspacing="1" class="box">
            <tr>
                <td width="246">
                    <input type="hidden" name="MAX_FILE_SIZE" value="2000000">
                    <input name="userfile" type="file" class="box" id="userfile">
                </td>
                <td width="80">
                    <input name="upload" type="submit" class="box" id="upload" value=" Upload ">
                </td>
            </tr>
        </table>
    </form>
</div>
</body>
</html>