<?php

$server 	= "localhost";
$username	= "root";
$password	= "";
$db 		= "test";
$koneksi = mysqli_connect($server, $username, $password, $db);

if(mysqli_connect_errno()) {
    echo "Koneksi gagal : ".mysqli_connect_error();
}

include '../library/config.php';
include '../library/opendb.php';

$query = "SELECT * FROM upload";
$result = mysqli_query($koneksi, $query) or die('Error, query failed');
?>
<html>
    <style>
        table, th, td {
          border:1px solid black;
        }
    </style>
<head>
    <title>Download File From MySQL</title>
</head>

<body>
    <table>
        <tr>
            <td>No</td>
            <td>Name</td>
            <td>Size</td>
            <td>Type</td>
            <td>Path</td>
        </tr>

    <?php
    if (mysqli_num_rows($result) == 0) {
        echo "Database is empty <br>";
    } else {
        while($row = mysqli_fetch_assoc($result)) {
            
    ?>

    <tr>
        <td><?=$row['id']?></td>
        <td><?=$row['name']?></td>
        <td><?=$row['size']?></td>
        <td><?=$row['type']?></td>
        <td>
            <a href="<?=$row['path']?>">View</a>
        </td>
    </tr>

    <?php

        }
    }
    include '../library/closedb.php';
    ?>
        
    </table>
</body>
</html>