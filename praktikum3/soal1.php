<?php

$kalimat = "Hai Dunia";
$counter = 0;

for ($i = 0; $i < strlen($kalimat); $i++) {
    if (isVocal($kalimat[$i])) {
        $counter++;
    }
}

echo "\"".$kalimat."\" ada ".$counter." huruf vokal";

function isVocal($char) {
    $vocals = array("a", "i", "u", "e", "o");

    for ($i=0; $i < 5; $i++) {
        if ($char == $vocals[$i]) {
            return true;
        } else {
            continue;
        }
    }
    return false;
}

?>