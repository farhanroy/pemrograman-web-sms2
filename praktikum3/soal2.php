<?php
$input1 = "123456790";
$input2 = "12a12ajpwer2";

$state = false;
for($i = 0; $i < strlen($input1); $i++) {
    if (isNumber($input1[$i])) {
        $state = true;
    } else {
        $state = false;
        break;
    }
}

if ($state) {
    echo "Selamat input1 valid </br>";    
} else {
    echo "Maaf input1 tidak valid </br>";    
}

$state2 = false;
for($i = 0; $i <= strlen($input2); $i++) {
    if (isNumber($input2[$i])) {
        $state = true;
    } else {
        $state = false;
        break;
    }
}

if ($state) {
    echo "Selamat input2 valid </br>";    
} else {
    echo "Maaf input2 tidak valid </br>";    
}

function isNumber($char) {
    if (ord($char) >= 48 && ord($char) <= 57) {
        return true;
    } else {
        return false;
    }
}

?>