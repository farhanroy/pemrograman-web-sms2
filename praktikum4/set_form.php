<!DOCTYPE html>
<html>
    <head>
        <title>Pemrograman Web</title>
    </head>
    <body>
        <form action="get_form.php" method="post">
            <div class="alert alert-dark" role="alert">
                <?= $_GET['alert'] ?? ''?>
            </div>
            <div class="mb-3">
                <label for="nama">Nama : </label>
                <input type="text" id="nama" ariadescribedby="name" name="nama" value="<?= $_GET['nama'] ?? ''?>"required>
            </div>
            <div class="mb-3">
                <label for="nilai">Nilai Angka : </label>
                <input type="number" id="nilai" name="nilai" value="<?= $_GET['nilai'] ?? ''?>" required>
            </div>
            <button type="submit">Submit</ button>
        </form>
    </body>
</html>