<?php

$nama = $_POST["nama"];
$nilai = $_POST["nilai"];

function isHuruf($name)
{
    $angka = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"];
    $name_char = str_split($name);
    foreach ($name_char as $char) {
        if (in_array($char, $angka)) {
            return 0;
        }
    }
    return 1;
}

function isAngka($nilai)
{
    $angka = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"];
    $nilai_char = str_split($nilai);
    foreach ($nilai_char as $char) {
        if (!in_array($char, $angka)) {
            return 0;
        }
    }
    return 1;
}

if (isHuruf($nama) && isAngka($nilai)) {
    $nilai = (int) $nilai;
    if ($nilai >= 0 && $nilai <= 100) {
        if ($nilai >= 0 && $nilai <= 40) {
            echo "Nama : $nama <br> Nilai Anda adalah E";
        } elseif ($nilai >= 41 && $nilai <= 55) {
            echo "Nama : $nama <br> Nilai Anda adalah D";
        } elseif ($nilai >= 50 && $nilai <= 61) {
            echo "Nama : $nama <br> Nilai Anda adalah C";
        } elseif ($nilai >= 60 && $nilai <= 65) {
            echo "Nama : $nama <br> Nilai Anda adalah BC";
        } elseif ($nilai >= 66 && $nilai <= 70) {
            echo "Nama : $nama <br> Nilai Anda adalah B";
        } elseif ($nilai >= 71 && $nilai <= 80) {
            echo "Nama : $nama <br> Nilai Anda adalah AB";
        } elseif ($nilai >= 81 && $nilai <= 100) {
            echo "Nama : $nama <br> Nilai Anda adalah A";
        }
    } else {
        echo "data hanya bisa diisi 0 - 100";
    }
} else {
    echo "nama harus berupa huruf";
}
?>
