<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Check Prime</title>
</head>
<body>
    <h1>Cek Bilangan Prima</h1>

    <form action="soal1.php" method="post">
        <input type="text" name="bilangan">
        <input type="submit" value="Submit">
    </form>

    <?php
        if (isset($_POST['bilangan'])) {    
            cekPrima($_POST['bilangan']);
        }

        function cekPrima($bil) {
            $angka = $bil;
            $hasil = true;
            for ($i = 2; $i < $angka; $i++)
            {
                if ($angka % $i == 0)
                    $hasil = false;
            }
            echo '<br/> Angka <b>'.$angka.'</b><br/>';
            echo $hasil ? 'Termasuk bilangan prima' : 'Bukan bilangan prima';
        }
    ?>

</body>
</html>