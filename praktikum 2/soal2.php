<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Nilai</title>
</head>
<body>
    <h1>Nilai Mahasiswa</h1>

    <form action="soal2.php" method="post">
        <label for="nama">Nama: </label>
        <input type="text" name="nama" style="margin-bottom: 10px"></br>
        <label for="nrp">NRP: </label>
        <input type="text" name="nrp" style="margin-bottom: 10px"></br>
        <label for="nilai">Nilai: </label>
        <input type="text" name="nilai" style="margin-bottom: 10px"></br>
        <input type="submit" value="Submit">
    </form>

    <?php
        if (isset($_POST['nilai'])) {
            echo "</br>";
            echo "Nama: ".$_POST['nama']."</br>";    
            echo "NRP: ".$_POST['nrp']."</br>";    
            nilaiKu($_POST['nilai']);
        }

        function nilaiKu ($nilai) {
            if ($nilai > 80 && $nilai <= 100)
                    echo "Nilai Anda A";
                else if ($nilai > 70 && $nilai <= 80)
                    echo "Nilai Anda AB";
                else if ($nilai > 65 && $nilai <= 70)
                    echo "Nilai Anda B";
                else if ($nilai > 60 && $nilai <= 65)
                    echo "Nilai Anda BC";
                else if ($nilai > 55 && $nilai <= 60)
                    echo "Nilai Anda C";
                else if ($nilai > 40 && $nilai <= 55)
                    echo "<font color=red> Nilai Anda D</font>";
                else if ($nilai >= 0 && $nilai <= 40)
                    echo "<font color=red> Nilai Anda E</font>";
                else
                    echo "Nilai Anda tidak valid";
        }
    ?>

</body>
</html>